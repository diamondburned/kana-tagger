module gitlab.com/diamondburned/kana-tagger

go 1.12

require (
	github.com/bogem/id3v2 v1.1.1
	github.com/dhowden/tag v0.0.0-20190519100835-db0c67e351b1
	gitlab.com/diamondburned/qprompt v0.0.0-20190615095937-ce92eca1719b
)
