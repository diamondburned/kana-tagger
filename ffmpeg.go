package main

import (
	"fmt"
	"os"
	"os/exec"
)

// ChangeArtist tells FFmpeg to change the artist name
func ChangeArtist(filename string, table [][2]string) error {
	var s = make([]string, len(table)*2+1)
	for i := 0; i < len(table); i++ {
		s[i*2] = "-metadata"
		s[i*2+1] = table[i][0] + "=" + table[i][1]
	}

	s[len(s)-1] = filename

	args := append([]string{
		"-nostdin", "-v", "fatal", "-y",
		"-i", filename,
		"-map_metadata", "0:s:0",
		"-write_id3v2", "1",
	}, s...)

	fmt.Printf("%#v\n", args)

	cmd := exec.Command("ffmpeg", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}
