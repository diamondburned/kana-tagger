package main

import (
	"strconv"

	"github.com/bogem/id3v2"
	"github.com/dhowden/tag"
)

// TagToID3v2 transfer all the metadata to ID3v2 tags.
func TagToID3v2(m tag.Metadata, tag *id3v2.Tag) {
	tag.SetAlbum(m.Album())
	tag.SetArtist(m.Artist())
	tag.SetGenre(m.Genre())
	tag.SetTitle(m.Title())
	tag.SetYear(strconv.Itoa(m.Year()))

	if comment := m.Comment(); comment != "" {
		tag.AddCommentFrame(id3v2.CommentFrame{
			Encoding: id3v2.EncodingUTF8,
			Language: "eng",
			Text:     comment,
		})
	}

	if lyrics := m.Lyrics(); lyrics != "" {
		tag.AddUnsynchronisedLyricsFrame(id3v2.UnsynchronisedLyricsFrame{
			Encoding: id3v2.EncodingUTF8,
			Language: "eng",
			Lyrics:   lyrics,
		})
	}

	if track, _ := m.Track(); track != 0 {
		tag.AddTextFrame(
			tag.CommonID("Track number/Position in set"),
			tag.DefaultEncoding(), strconv.Itoa(track),
		)
	}

	tag.AddTextFrame(
		tag.CommonID("Composer"),
		tag.DefaultEncoding(), m.Composer(),
	)

	tag.AddTextFrame(
		tag.CommonID("File type"),
		tag.DefaultEncoding(), string(m.FileType()),
	)
}
